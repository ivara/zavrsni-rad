const environment = process.env.NODE_ENV || 'development';
const knexConfig = require('./../../jobo-db/knexfile');
const environmentConfig = knexConfig[environment];
const knex = require('knex');
const connection = knex(environmentConfig);

console.log("Connected to db");

module.exports = connection;
