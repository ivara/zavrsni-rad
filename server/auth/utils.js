const jwt  = require('jsonwebtoken');
const users = require('../queries/users');

function create(user){
    return new Promise((resolve, reject) => {
        jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '7d' }, (error, token) => {
            if(error) return reject(error); 
            console.log("Created token for user: ", user.email);
            resolve(token);
        });
    });
}

function verify(token){
    return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.TOKEN_SECRET, (error, payload) => {
            console.log("Verifying token");
            if(error) return reject(error);
            resolve(payload);
        });
    });
}

module.exports = {
    create,
    verify,
};
