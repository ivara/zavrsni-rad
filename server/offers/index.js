const express = require('express');
const router = express.Router();

const offers = require('../queries/offers');
const categories = require('../queries/categories');
const jobs = require('../queries/jobs');
const requests = require('../queries/requests');
const { checkAuth } = require('../middlewares');


router.get('/:id', async(req,res,next) => {
    try {
        const data = await offers.getAll(req.params.id);
        console.log("got offers");
        res.json(data);
    } catch (error) {
        next(error);
    }
});


router.get('/user/:id', async(req,res,next) => {
    try {
        const data = await offers.getUserOffers(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
});

router.get('/get/:id', async (req, res, next) => {
    try{
        const data = await offers.findById(req.params.id);
        res.send(data);
    } catch(error) {
        console.log("Error: ", error);
        next(error);
    }
});

router.get('/requests/:id', async(req,res,next) => {
    try {
        const data = await offers.getOfferRequests(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
});

router.post('/request/add', async (req, res, next) => {
    try {
        const request = await requests.insert(req.body);
        console.log("Added new request: ", request);

        if(!request.error){
            return res.status(200).json({
                title: "Request added",
            })
        } else {
            return res.status(404).json({
                title: "Error",
            })
        }      
    } catch (error) {
        console.log("Error: ", error);
        return res.status(401).json({
            title: "Error",
        })
    }
});

router.post('/add', async (req, res, next) => {
    try {
        newOffer = req.body;
        categoryId = await categories.getByName(req.body.job_category_id);
        newOffer["created_at"] = new Date().toISOString();
        newOffer["job_category_id"] = categoryId.id;
        const offer = await offers.insert(newOffer);
        console.log("Added new offer: ", offer);
        await categories.updateOffersNumber(categoryId.id);

        console.log(newOffer.user_id)
        fetchedJobs = await jobs.getAll(newOffer.user_id);
        console.log(fetchedJobs);
        //automatic pairing - if new offer has the same category as one of the active jobs automatically create request
        for(var i = 0; i < fetchedJobs.length; i++){
            console.log("current job", fetchedJobs[i]);
            console.log("going through jobs, current comparing: ", fetchedJobs[i], newOffer.job_category_id);
            if(fetchedJobs[i].job_category_id == newOffer.job_category_id){
                console.log("matched");
                newRequest = {
                    offer_id: offer.id,
                    user_id: fetchedJobs[i].poster_id,
                    message: 'This offer was created automatically by Jobo because we thought the user might be interested. Message the user for more information.',
                }
                requests.insert(newRequest);
            }
        }

        if(!offer.error){
            return res.status(200).json({
                title: "Offer added",
            })
        } else {
            return res.status(404).json({
                title: "Error",
            })
        }      
    } catch (error) {
        console.log("Error: ", error);
        return res.status(401).json({
            title: "Error",
        })
    }
});

router.post('/accept', async (req, res, next) => {
    try {
        console.log(req.body);
        const job = await jobs.insert(req.body.newJob);
        const category = await categories.updateJobsNumber();
        console.log("Added new job: ", job);
        const request = await requests.accept(req.body.requestId);
        if(!job.error && request){
            return res.status(200).json({
                title: "Accepted",
            })
        }
    } catch (error) {
        console.log("Error: ", error);
        return res.status(400).json({
            title: "Error",
        })
    }
});

router.post('/delete', async(req,res,next) => {
    try {
        const data =  await offers.delete(req.body.offerId);
        const datacategories = await categories.updateOffersNumberDown(req.body.categoryId);
        res.json(data);
    } catch (error) {
        next(error);
    }
});







module.exports = router;