const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const bcrypt = require('bcryptjs');
const users = require('./queries/users');
const jobqueries = require('./queries/jobs');
const applications = require('./queries/applications');
const { create, verify } = require('./auth/utils');



require('dotenv').config();
var app = express();
const auth = require('./auth');
const jobs = require('./jobs');
const profile = require('./profile');
const offers = require('./offers');
const { checkAuth , checkAuthHeaderSetUser, errorHandler } = require('./middlewares');

app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());


app.use(checkAuthHeaderSetUser);


app.get('/', (req, res) => {
    res.json({
        message: 'Ponuda i potražnja kratkotrajnih poslova'
    });
});

app.use('/auth', auth);
app.use('/jobs', jobs);
app.use('/profile', profile);
app.use('/offers', offers);
app.use(errorHandler);


app.post('/register', async (req, res, next) => {
    const today = new Date().toISOString();
    var salt = bcrypt.genSaltSync(10);
    const newUser = {
        email: req.body.email,
        first_name: req.body.name,
        last_name: req.body.lastname,
        joined_at: today,
        password: bcrypt.hashSync(req.body.password, salt),
    }
    //check if there is already a registered user with given email
    alreadyExists = await users.findByEmail(req.body.email);
    if(alreadyExists){
        //check if user previously registered with google
        //if yes update user, add password
        if(alreadyExists.google_id) {
            newUser.joined_at = alreadyExists.joined_at;
            user = await users.update(alreadyExists.id, newUser);
            console.log("updated user ", user.email);
            return res.status(200).json({
                title: "register with email success",
            }); 
        }
        //if not, display error message
        else {
            return res.status(400).json({
                title: "error",
                error: "Email already in use!",
            });
        }
    }
    user = await users.insert(newUser);
    console.log("added new user: ", user);
    return res.status(200).json({
        title: "register success",
    });

});

app.post('/login', async (req, res, next) => {
    user = await users.findByEmail(req.body.email);
    //incorrect email
    if(!user) {
        return res.status(401).json({
            title: "log in failed",
            error: "Inalid credentials!",
        })
    }
    if(!user.password){
        return res.status(401).json({
            title: "log in failed",
            error: "You previously registered with Google, please log in with Google or register with email!",
        });
    }
    //incorrect password
    if(!bcrypt.compareSync(req.body.password, user.password)){
        return res.status(401).json({
            title: "log in failed",
            error: "Invalid credentials!",
        })
    }
    console.log("user logged in with email: ", user.email);
    try{
        const token = await create(user);
        return res.status(200).json({
            title: "Login success",
            token: token,
        });
    } catch(error){
        res.redirect(`${process.env.CLIENT_ERROR_REDIRECT}${error.message}`);
    }
});

app.get('/homepage/:id', async (req, res) => {
    try {
        const userJobs = await jobqueries.getUserJobs(req.params.id);
        const userApplications = await applications.getById(req.params.id)
        res.json({
            jobs: userJobs,
            applications: userApplications,
        });
    } catch (error) {
        console.log(error);
        return res.status(401).json({
            error: error,
        });
    }
});


//if deployed
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Listening on ${port}`);
});