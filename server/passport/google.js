const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const users = require('../queries/users');


passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "/auth/google/callback"
  },
  async function(accessToken, refreshToken, profile, cb) {
    const email = profile.emails[0].value;
    let today = new Date().toISOString().slice(0, 10);
    const googleUser = {
        first_name: profile.name.givenName,
        last_name: profile.name.familyName,
        email,
        joined_at: today,
        google_id: profile.id,
    };
    try {
        let user = await users.findByEmail(email);
        if(user){
            //update user
            console.log('user already has an account, found by email', user.email);
            googleUser.joined_at = user.joined_at;
            user = await users.update(user.id, googleUser);
        } else{
            // insert user
            user = await users.insert(googleUser);
            console.log("added new user registered with google ", googleUser)
        }
        return cb(null, user);
    } catch (error) {
        return cb(error);
    }
  }
));
