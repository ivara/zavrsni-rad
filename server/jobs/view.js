const express = require('express');
const router = express.Router();
const jobs = require('../queries/jobs');
const applications = require('../queries/applications');
const {checkAuth, checkAuthHeaderSetUser} = require('../middlewares');


router.get('/:id', async (req, res, next) => {
    try{
        const job = await jobs.findById(req.params.id);
        res.send(job);
    } catch(error) {
        console.log("Error: ", error);
        next(error);
    }
})

router.post('/:id', checkAuth, async (req, res, next) => {
    try {
        const application = await applications.insert(req.body);
        console.log("Added new application: ", application);        
    } catch (error) {
        console.log("Error: ", error);
        next(error);
    }
})



module.exports = router;