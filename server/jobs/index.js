const express = require('express');
const router = express.Router();
const addNew = require('./add');
const view = require('./view');
const application = require('./application');
const jobs = require('../queries/jobs');
const categories = require('../queries/categories');
const users = require('../queries/users');
const { checkAuth } = require('../middlewares');


router.use('/add', addNew);
router.use('/view', view);
router.use('/application', application);


router.get('/', async (req, res, next) => {
    try {
        const allCategories = await categories.getAll();
        res.json(allCategories);
    } catch (error) {
        next(error);
    }
});

router.get('/:id', async(req,res,next) => {
    try {
        const data = await jobs.getAll(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
});

router.get('/user/:id', async(req,res,next) => {
    try {
        const data = await jobs.getUserJobs(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
});

router.post('/close/job', async(req, res, next) => {
    try {
        const job = await jobs.close(req.body.jobId);
        const user = await users.close(req.body.userId, req.body.rating);
        const category = await categories.close(req.body.categoryId);
    } catch (error) {
        next(error);
    }
});

router.post('/delete', async(req,res,next) => {
    try {
        const data =  await jobs.delete(req.body.jobId);
        const datacategories = await categories.close(req.body.categoryId);
        res.json(data);
    } catch (error) {
        next(error);
    }
});

module.exports = router;