const express = require('express');
const router = express.Router();
const applications = require('../queries/applications');
const jobs = require('../queries/jobs');

const {checkAuth} = require('../middlewares');


router.get('/:id', async(req,res,next) => {
    try {
        const data = await applications.getAll(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
})

router.get('/applied/:userid/:jobid', async(req,res,next) => {
    try {
        const data = await applications.getUserApplicationByJob(req.params.userid, req.params.jobid);
        res.json(data);;
    } catch (error) {
        next(error);
    }
})

router.get('/user/:id', async(req,res,next) => {
    try {
        const data = await applications.getUserApplications(req.params.id);
        res.json(data);
    } catch (error) {
        next(error);
    }
})


router.post('/', async (req, res, next) => {
    try {
        const application = await applications.insert(req.body);
        console.log("Added new application: ", application);
        return res.status(200).json({
            title: "Application added",
        })       
    } catch (error) {
        console.log("Error: ", error);
        next(error);
    }
});

router.post('/asign', async (req, res, next) => {
    try {
        const application = await applications.asignToUser(req.body.applicationId);
        const job = await jobs.asignWorker(req.body.userId, req.body.jobId);
        console.log("Asigned job to user", application);
        return res.status(200).json({
            title: "Job assigned",
        })       
    } catch (error) {
        next(error);
    }
});

module.exports = router;