const express = require('express');
const router = express.Router();
const jobs = require('../queries/jobs');
const users = require('../queries/users');
const categories = require('../queries/categories');
const offers = require('../queries/offers');
const applications = require('../queries/applications');
const requests = require('../queries/requests');
const {checkAuth} = require('../middlewares');


router.post('/', async (req, res, next) => {
    try {
        const category = await categories.getByName(req.body.job_category_id);
        console.log(category)
        newJob = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            address: req.body.address,
            created_at: new Date().toISOString(),
            poster_id: req.body.poster_id,
            job_category_id: category.id,
            city: req.body.city
        }
        const job = await jobs.insert(newJob);
        const user = await users.updateJobsNumber(req.body.poster_id);
        const categoryUpdate = await categories.updateJobsNumber(category.id);
        console.log("Added new job: ", job);
        
        //automatic pairing - if new job has the same category as one of the open offers automatically create application
        if(!job.error) {
            const allOffers = await offers.getAll(newJob.poster_id);
            for (var i=0; i<allOffers.length; i++) {
                if(allOffers[i].job_category_id == newJob.job_category_id) {
                    newApplication = {
                        job_id: job.id,
                        user_id: allOffers[i].user_id,
                        message: "Jobo found this job that might fit in with your offer. Go to user's profile and contact him for more information.",
                    }
                    const reqResult = applications.insert(newApplication);
                }
            }
        }



        if(job && !user && !reqResult){
            return res.status(200).json({
                title: "Job successfully added",
            }) 
        }  else{
            return res.status(401).json({
                title: "Error"
            })
        }  
    } catch (error) {
        console.log("Error: ", error);
        next(error);
    }
    
})

module.exports = router;