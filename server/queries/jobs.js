const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

const { insertIntoDB } = require('./index');

const schema = Joi.object({
    name: Joi.string()
        .required(),
    description: Joi.string()
        .required(),
    price: Joi.required(),
    created_at: Joi.date()
        .required(),
    poster_id: Joi.number()
        .required(),
    worker_id: Joi.number(),
    address: Joi.string().allow('', null),
    city: Joi.string().allow('', null),
    job_category_id: Joi.number().required(),
});


// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    async insert(job){
        try {
            return insertIntoDB('job', job, schema)
        } catch (error) {
            console.log(error);
        }
        
    },
    async getAll(id) {
        return db('job').where('completed', false)
            .whereNot('poster_id', id);
    },
    async findById(id) {
        return db('job').where('id', id).first();
    },
    async asignWorker(userid, jobid) {
        return db('job').where('id', jobid).update('worker_id', userid);
    },
    async getUserJobs(id) {
        return db('job').where({
            'poster_id': id,
            'completed': false,
        });
    },

    async close(id) {
        return db('job').where('id', id).update('completed', true);
    },

    async delete(id) {
        return db('job').where('id', id).del();
    },

}