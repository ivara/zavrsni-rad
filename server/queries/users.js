const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

const { insertIntoDB } = require('./index');

const schema = Joi.object({
    first_name: Joi.string()
        .min(2)
        .max(30)
        .required(),
    last_name: Joi.string()
        .required(),
    password: Joi.string(),
    dob: Joi.date(),
    email: Joi.string()
        .email({ minDomainSegments: 2}).required(),
    gender: Joi.string(),
    joined_at: Joi.date().required(),
    google_id: Joi.string(),
});


// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    findByEmail(email){
        return db('users').where('email', email).first();
    },
    findById(id){
        return db('users').where('id', id).first();
    },
    insert(user){
        return insertIntoDB('users', user, schema);
    },
    async update(id, user){
        const rows = await db('users').where('id', id).update(user, '*');
        return rows[0];
    },
   async updateJobsNumber(id){
        const rows = await db('users').where('id', '=', id).increment('jobs_posted',1);
        return rows[0];
   },
   async close(user, rating){
        return db('users').where('id', user).increment({
            rating: rating,
            jobs_done: 1,
        });
   }

}