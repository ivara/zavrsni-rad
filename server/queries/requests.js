const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

const { insertIntoDB } = require('./index');

const schema = Joi.object({
    offer_id: Joi.number()
        .required(),
    user_id: Joi.number()
        .required(),
    message: Joi.string().required(),
    price: Joi.string().allow('', null),
});


// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    async insert(request){
        try {
            return insertIntoDB('offer_request', request, schema)
        } catch (error) {
            console.log(error);
        }
    },

    async accept(id){
        try {
            return db('offer_request').where('id', id).update('accepted', true);
        } catch (error) {
            console.log(error);
        }
    },


    
}