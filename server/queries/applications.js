const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

const { insertIntoDB } = require('./index');

const schema = Joi.object({
    job_id: Joi.number()
        .required(),
    user_id: Joi.number()
        .required(),
    message: Joi.string().required(),
});


// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    async insert(application){
        try {
            return insertIntoDB('job_application', application, schema)
        } catch (error) {
            console.log(error);
        }
        
    },
    async getAll(jobid){
        try {
            return db('job_application')
                .join('users', 'job_application.user_id', 'users.id')
                .select('job_application.id', 'job_id', 'user_id', 'message', 'selected', 'first_name', 'last_name')
                .where('job_id', jobid); 
        } catch (error) {
            next(error);
        }
    },
    async getUserApplications(userid){
        try {
            return db('job_application')
                .join('job', 'job_application.job_id', '=', 'job.id')
                .select('job_application.id', 'job_id', 'user_id', 'message', 'name', 'selected')
                .where({
                    'user_id': userid,
                    'completed': false,
                }); 
        } catch (error) {
            next(error);
        }
    },
    async getUserApplicationByJob(userid, jobid) {
        try {
            return db('job_application').where({
                'job_id': jobid,
                'user_id': userid,
            }); 
        } catch (error) {
            next(error);
        }
    },

    async asignToUser(id) {
        try{
            return db('job_application')
                .where('id', id)
                .update('selected', true);
        } catch(error){
            next(error);
        }
    }
    
}