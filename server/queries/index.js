const db = require('../db/connection.js');
const Joi = require('@hapi/joi');


async function insertIntoDB(table_name, item, schema){
    
    const result = schema.validate(item);
    if(result.error){
        return Promise.reject(result.error);
    }
    else {
        const rows = await db(table_name).insert(item, '*');
        return rows[0];
    }
}

module.exports = {
    insertIntoDB,
}