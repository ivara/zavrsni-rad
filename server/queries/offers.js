const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

const { insertIntoDB } = require('./index');

const schema = Joi.object({
    name: Joi.string()
        .required(),
    description: Joi.string()
        .required(),
    created_at: Joi.date()
        .required(),
    user_id: Joi.number()
        .required(),
    job_category_id: Joi.number().required(),
});


// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    async insert(offer){
        try {
            return insertIntoDB('offer', offer, schema)
        } catch (error) {
            console.log(error);
        }
        
    },
    async getAll(id) {
        return db('offer').where('active', true)
            .whereNot('user_id', id);
    },
    async findById(id) {
        return db('offer').where('id', id).first();
    },

    async getUserOffers(id) {
        return db('offer').where({
            'user_id': id,
            'active': true,
        });
    },

    async close(id) {
        return db('offer').where('id', id).update('active', false);
    },

    async getOfferRequests(id) {
        return db('offer_request').join('users', 'offer_request.user_id', '=', 'users.id')
        .select('offer_request.id', 'offer_id', 'user_id', 'message', 'price', 'accepted', 'first_name', 'last_name')
        .where('offer_id', id); 
    },

    async getAllActive() {
        return db('offer').where('active', true)
    },

    async delete(id) {
        return db('offer').where('id', id).del();
    },
    
}