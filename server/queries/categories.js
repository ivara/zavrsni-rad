const db = require('../db/connection.js');
const Joi = require('@hapi/joi');

// If the input is valid, then the error will be undefined.
//If the input is invalid, error is assigned a ValidationError object providing more information.

module.exports = {
    async getAll() {
        return db('job_category').select();
    },
    async getByName(name) {
        return db('job_category').where('name', name).first();
    },
    async updateJobsNumber(id) {
        return db('job_category').where('id', id).increment('jobs_number', 1);
    },
    async close(id) {
        return db('job_category').where('id', id).decrement('jobs_number', 1);
    },
    async updateOffersNumber(id){
        return db('job_category').where('id', id).increment('offers_number', 1);
    },
    async updateOffersNumberDown(id){
        return db('job_category').where('id', id).decrement('offers_number', 1);
    },

}