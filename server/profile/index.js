const express = require('express');
const router = express.Router();
const users = require('../queries/users');
const { checkAuth } = require('../middlewares');



router.get('/:id', async (req, res, next) => {
    try {
        const result = await users.findById(req.params.id);
        res.json(result);
    } catch (error) {
        next(error);
    }
});

router.post('/', async (req, res, next) => {
    user = await users.update(req.body.id, req.body);
    console.log("Edited user");
    res.json(user);
});

module.exports = router;