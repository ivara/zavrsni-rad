const { verify } = require('../auth/utils')

async function checkAuth(req, res, next) {
    console.log("checking auth got: ", req.get('authorization'))
    const authorization = req.get('authorization');
    if(authorization){
        const token = authorization.split(' ')[1];
        try{
            console.log("verifying with token: ", token);
            const user = await verify(token);
            console.log("User after verifying: ", user);
            if(user){
              return next();
            }
            //req.user = user;
        } catch (error) {
            console.error(error);
        } 
    }
    res.status(401);
    next(new Error('Unauthorized'));
}

async function checkAuthHeaderSetUser(req, res, next) {
    const authorization = req.get('authorization');
    if (authorization) {
      const token = authorization.split(' ')[1];
      try {
        const user = await verify(token);
        req.user = user;
      } catch (error) {
        console.error(error);
      }
    }
    next();
  }

  function errorHandler(error, req, res, next) {
    console.log(error);
    res.status(res.statusCode || 500);
    res.json({
      message: error.message,
      error: process.env.NODE_ENV === 'production' ? {} : error.stack,
    });
  }

module.exports = {
    checkAuth,
    checkAuthHeaderSetUser,
    errorHandler,
};

