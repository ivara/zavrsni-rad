
exports.up = function(knex) {
    return knex.schema.createTable('job', function(table){
      table.increments('id');
      table.text('name').notNullable();
      table.text('description').notNullable();
      table.decimal('price', 2).notNullable();
      table.boolean('completed').default(0);
      table.text('location').notNullable();
      table.datetime('created_at').notNullable();
    });
  };
  
  exports.down = function(knex) {
    return knex.schema.dropTable('job');
  };
  