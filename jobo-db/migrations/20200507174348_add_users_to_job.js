
exports.up = function(knex) {
  return knex.schema.table('job', function(table){
      table.integer('poster_id').unsigned().notNullable().references('id').inTable('users');
      table.integer('worker_id').unsigned().references('id').inTable('users');
      table.integer('job_category_id').unsigned().references('id').inTable('job_category');
  })
};

exports.down = function(knex) {
    return knex.schema.table('job', function(table){
        table.dropColumn('poster_id');
        table.dropColumn('worker_id');
        table.dropColumn('job_category_id');
    })
};
