
exports.up = function(knex) {
    return knex.schema.createTable('job_category', function(table){
        table.increments('id');
        table.string('name', 255).notNullable();
        table.text('description').notNullable();
      });
};

exports.down = function(knex) {
    return knex.schema.dropTable('job_category');
};
