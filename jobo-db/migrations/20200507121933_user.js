
exports.up = function(knex) {
  return knex.schema.createTable('users', function(table){
    table.increments('id');
    table.string('email', 255).notNullable();
    table.string('first_name', 255).notNullable();
    table.string('last_name', 255).notNullable();
    table.string('gender', 1).notNullable();
    table.string('phone_number', 255);
    table.integer('jobs_posted').default(0);
    table.integer('jobs_done').default(0);
    table.integer('rating').default(0);
    table.date('dob').notNullable();
    table.date('joined_at');
    table.text('google_id');
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('users');
};
