
exports.seed = async function(knex, Promise) {
  await knex('job_category').del();
  await knex.raw('ALTER SEQUENCE job_category_id_seq restart with 7;')
  return knex('job_category').insert([
    {id: 1, name: 'Tutoring'},
    {id: 2, name: 'Cleaning'},
    {id: 3, name: 'Techology'},
    {id: 4, name: 'Animals'},
    {id: 5, name: 'Around the house'},
    {id: 6, name: 'Heavy lifting'},
    {id: 7, name: 'Other'}
  ]);
};
