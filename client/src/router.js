import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import LoginToken from './views/LoginToken.vue';
import Homepage from './views/Homepage.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Profile from './views/Profile.vue';
import Jobs from './views/Jobs.vue';
import AddNewJob from './views/AddNewJob.vue';
import ViewJob from './views/ViewJob.vue';
import Offers from './views/Offers.vue';
import AddNewOffer from './views/AddNewOffer.vue';
import ViewOffer from './views/ViewOffer.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/login/token/:token',
    name: 'login-token',
    component: LoginToken,
  },
  {
    path: '/homepage',
    name: 'Homepage',
    component: Homepage,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/register',
    name: Register,
    component: Register,
  },
  {
    path: '/profile/:id',
    name: Profile,
    component: Profile,
  },
  {
    path: '/jobs',
    name: Jobs,
    component: Jobs,
  },
  {
    path: '/jobs/add',
    name: AddNewJob,
    component: AddNewJob,
  },
  {
    path: '/jobs/view/:id',
    name: ViewJob,
    component: ViewJob,
  },
  {
    path: '/offers',
    name: Offers,
    component: Offers,
  },
  {
    path: '/offers/add',
    name: AddNewOffer,
    component: AddNewOffer,
  },
  {
    path: '/offers/view/:id',
    name: ViewOffer,
    component: ViewOffer,
  },
];

const router = new VueRouter({
  routes,
  mode:"history",
});

export default router;
