import Vue from 'vue';
import Vuex from 'vuex';

import {getUserById, getAllCategories, getApplications, getUserApplications, createJob, createApplication, getJobById, getJobs, getUserJobs, checkIfApplied, getOffers, getUserOffers, getOfferById, getRequestsForOffer } from './API';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: '',
    user: null,
    categories: [],
    userJobs: [],
    jobs: [],
    applications: [],
    job: null,
    hasApplied: false,
    viewUser: null,
    offers: [],
    userOffers: [],
    offer: null,
    requests: [],
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    setUser(state, user) {
      state.user = user;
    },
    setCategories(state, categories){
      state.categories = categories;
    },
    setUserJobs(state, jobs) {
      state.userJobs = jobs;
    },
    setJobs(state, jobs) {
      state.jobs = jobs;
    },
    setApplications(state, applications) {
      state.applications = applications;
    },
    pushJob(state, job) {
      state.jobs.push(job);
    },
    pushApplication(state, application) {
      state.applications.push(application);
    },
    setJob(state, job) {
      state.job = job;
    },
    setApplied(state, applied) {
      state.hasApplied = applied;
    },
    setUserView(state, user) {
      state.viewUser = user;
    },
    setOffers(state, offers){
      state.offers = offers;
    },
    setUserOffers(state, offers) {
      state.userOffers = offers;
    },
    setOffer(state, offer){
      state.offer = offer;
    },
    setRequests(state, requests) {
      state.requests = requests;
    }
   
  },
  actions: {
    async login({ commit }, token) {
      commit('setToken', token);
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
      const user = JSON.parse(jsonPayload);
      commit('setUser', user);
    },
    async getCategories({ commit }) {
      const categories = await getAllCategories();
      commit('setCategories', categories);
    },
    async addJob({ commit }, job) {
      const newJob = await createJob(job);
      commit('pushJob', newJob);
    },
    async addApplication( {commit }, newApplication) {
      const application = await createApplication(newApplication);
      commit('pushApplication', application);
    },
    async getUserJobs({ commit }, userid) {
      const jobs = await getUserJobs(userid);
      commit('setUserJobs', jobs);
    },
    async getAllJobs({commit}, userid) {
      const jobs = await getJobs(userid);
      commit('setJobs', jobs);
    },
    async setCurrentJob( {commit}, id) {
      const job = await getJobById(id);
      commit('setJob', job);
    },
    async getAllApplications({ commit }, jobid) {
      console.log("store")
      const applications = await getApplications(jobid);
      commit('setApplications', applications);
    },
    async getUserApplications({ commit }, id) {
      const applications = await getUserApplications(id);
      commit('setApplications', applications);
    },
     async checkIfApplied({commit}, payload) {
       const result = await checkIfApplied(payload.userid, payload.jobid);
       if(result.length>0){
          commit('setApplied', true);
       } else {
         commit('setApplied', false);
       }
     },
     async getUserById({commit}, id) {
       const result = await getUserById(id);
       commit('setUserView', result) ;
     },
     async getAllOffers({commit}, id){
       const offers = await getOffers(id);
       commit('setOffers', offers);
     },
     async getUserOffers({ commit }, userid) {
      const offers = await getUserOffers(userid);
      commit('setUserOffers', offers);
    },
    async setCurrentOffer( {commit}, id) {
      const offer = await getOfferById(id);
      commit('setOffer', offer);
    },
    async getRequests({commit}, id) {
      const requests = await getRequestsForOffer(id);
      commit('setRequests', requests);
    }
  },

});