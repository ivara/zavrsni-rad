const BASE_URL = 'http://localhost:3000/';

export async function getAllCategories() {
    const response = await fetch(`${BASE_URL}jobs`);
    const categories = await response.json();
    return categories;
}

export async function getApplications(jobid) {
    const response = await fetch(`${BASE_URL}jobs/application/${jobid}`);
    const data = await response.json();
    return data;
}

export async function getUserApplications(userid) {
    const response = await fetch(`${BASE_URL}jobs/application/user/${userid}`);
    const data = await response.json();
    return data;
}

//extra?
export async function createJob(job) {
    const response = await fetch(`${BASE_URL}jobs/add`, {
        method: 'POST',
        body: JSON.stringify(job),
        headers: {
            'content-type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem("token"),
        },
    })
}

//extra?
export async function createApplication(application) {
    const response = await fetch(`${BASE_URL}jobs/view/:id`, {
        method: 'POST',
        body: JSON.stringify(application),
        headers: {
            'content-type': 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem("token"),
        },
    })
}

export async function getJobById(id) {
    const response = await fetch(`${BASE_URL}jobs/view/${id}`);
    const data = await response.json();
    return data;
}

//get all jobs that are active and are not posted by user 
export async function getJobs(userid) {
    const response = await fetch(`${BASE_URL}jobs/${userid}`);
    const jobs = await response.json();
    return jobs; 
}

//get all jobs posted by user
export async function getUserJobs(userid) {
    const response = await fetch(`${BASE_URL}jobs/user/${userid}`);
    const jobs = await response.json();
    return jobs;
}

export async function checkIfApplied(userid, jobid) {
    const response = await fetch(`${BASE_URL}jobs/application/applied/${userid}/${jobid}`);
    const jobs = await response.json();
    return jobs;
}

export async function getUserById(id) {
    const response = await fetch(`${BASE_URL}profile/${id}`);
    return response.json();
}

export async function getOffers(id){
    const response = await fetch(`${BASE_URL}offers/${id}`);
    return response.json();
}

export async function getUserOffers(userid) {
    const response = await fetch(`${BASE_URL}offers/user/${userid}`);
    const offers = await response.json();
    return offers;
}

export async function getOfferById(id) {
    const response = await fetch(`${BASE_URL}offers/get/${id}`);
    return response.json();
}

export async function getRequestsForOffer(id) {
    const response = await fetch(`${BASE_URL}offers/requests/${id}`);
    return response.json();
}




